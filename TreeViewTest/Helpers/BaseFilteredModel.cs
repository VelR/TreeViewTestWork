﻿namespace TreeViewTest.Helpers
{
    /// <summary>
    /// Class extended the model for filtering.
    /// </summary>
    public class BaseFilteredModel
    {
        /// <summary>
        /// The flag passed the element by the filter condition.
        /// </summary>
        public bool IsFiltred { get; set; }
    }
}
