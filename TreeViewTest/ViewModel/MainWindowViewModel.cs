﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using TreeViewTest.Helpers;
using TreeViewTest.Interfaces;
using TreeViewTest.Model;
using TreeViewTest.Services;

namespace TreeViewTest.ViewModel
{
    /// <summary>
    /// ViewModel of MainWindow.
    /// </summary>
    class MainWindowViewModel : DependencyObject
    {
        #region Fields

        /// <summary>
        /// Service of work with categories.
        /// </summary>
        private readonly ICategoriesService _categoriesService;

        #endregion

        #region Dependency Properties

        /// <summary>
        /// Expand all TreeViewItems.
        /// </summary>
        public bool IsExpandAll
        {
            get { return (bool)GetValue(IsExpandAllProperty); }
            set { SetValue(IsExpandAllProperty, value); }
        }

        public static readonly DependencyProperty IsExpandAllProperty =
            DependencyProperty.Register("IsExpandAll", typeof(bool), typeof(MainWindowViewModel), new PropertyMetadata(false));

        /// <summary>
        /// Сollection displayed in the tree of elements.
        /// </summary>
        public ICollectionView TreeViewItemsSource
        {
            get => (ICollectionView)GetValue(TreeViewItemsSourceProperty);
            set => SetValue(TreeViewItemsSourceProperty, value);
        }

        public static readonly DependencyProperty TreeViewItemsSourceProperty =
            DependencyProperty.Register(nameof(TreeViewItemsSource), typeof(ICollectionView), typeof(MainWindowViewModel), new PropertyMetadata(null));

        /// <summary>
        /// Filter text entered by the user.
        /// </summary>
        public string FilterText
        {
            get => (string)GetValue(FilterTextProperty);
            set => SetValue(FilterTextProperty, value);
        }

        public static readonly DependencyProperty FilterTextProperty =
            DependencyProperty.Register(nameof(FilterText), typeof(string), typeof(MainWindowViewModel), new PropertyMetadata(string.Empty, FilterTextPropertyChangedCallback));

        private static void FilterTextPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var viewModel = d as MainWindowViewModel;
            viewModel?.ApplyFilter();
        }

        #endregion

        #region Constructor

        /// <summary>
        /// ViewModel of MainWindow.
        /// </summary>
        public MainWindowViewModel()
        {
            _categoriesService = new CategoriesService();
            TreeViewItemsSource = CollectionViewSource.GetDefaultView(_categoriesService.Categories);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Apply a filter to the tree.
        /// </summary>
        private void ApplyFilter()
        {
            var filterText = FilterText.Trim();
            var isFilterTextNotEmpty = filterText != string.Empty;
            // Categories Filter
            TreeViewItemsSource.Filter += c =>
            {
                var category = (Category)c;
                var filterResult = category.Name.ToLower().Contains(filterText);

                // Items Filter
                var items = CollectionViewSource.GetDefaultView(category.Items);
                if (items != null)
                {
                    var result = filterResult;
                    items.Filter += i =>
                    {
                        var item = (Item)i;
                        var itemFilterResult = item.Name.ToLower().Contains(filterText);
                        item.IsFiltred = isFilterTextNotEmpty && itemFilterResult;
                        return result || itemFilterResult;
                    };
                    category.IsFiltred = isFilterTextNotEmpty && filterResult;
                    if (!items.IsEmpty)
                    {
                        filterResult = true;
                    }
                }
                return filterResult;
            };
            IsExpandAll = isFilterTextNotEmpty && !TreeViewItemsSource.IsEmpty;
        }

        #endregion

        #region Command

        /// <summary>
        /// Command to fill a tree.
        /// </summary>
        public RelayCommand LoadCommand =>
            new RelayCommand(obj =>
            {
                _categoriesService.Add(25);
                if (_categoriesService.Categories.Count == 25)
                {
                    ApplyFilter();
                }
            });

        #endregion
    }
}
