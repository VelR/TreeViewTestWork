﻿using System.Collections.ObjectModel;
using TreeViewTest.Model;

namespace TreeViewTest.Interfaces
{
    /// <summary>
    /// Interface to work with the categories.
    /// </summary>
    public interface ICategoriesService
    {
        /// <summary>
        /// Returns all added categories.
        /// </summary>
        ObservableCollection<Category> Categories { get; }

        /// <summary>
        /// Add category.
        /// </summary>
        void Add();

        /// <summary>
        /// Adding more categories.
        /// </summary>
        /// <param name="number">Number of categories added</param>
        void Add(int number);
    }
}
