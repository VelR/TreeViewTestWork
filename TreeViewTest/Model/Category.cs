﻿using System.Collections.Generic;
using TreeViewTest.Helpers;

namespace TreeViewTest.Model
{
    /// <summary>
    ///  Category model.
    /// </summary>
    public class Category: BaseFilteredModel
    {
        #region Props

        /// <summary>
        /// Category name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Category Id.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Items of a category.
        /// </summary>
        public IEnumerable<Item> Items { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Category model.
        /// </summary>
        public Category(int id)
        {
            Id = id;
            Name = $"Category{id}";
        }

        #endregion
    }
}
