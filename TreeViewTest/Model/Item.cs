﻿using TreeViewTest.Helpers;

namespace TreeViewTest.Model
{
    /// <summary>
    /// Item model.
    /// </summary>
    public class Item : BaseFilteredModel
    {
        #region Props
        
        /// <summary>
        /// Item name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Item Id.
        /// </summary>
        public int Id { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Item model.
        /// </summary>
        public Item(int id)
        {
            Id = id;
            Name = $"Item{id}";
        }

        #endregion
    }
}
