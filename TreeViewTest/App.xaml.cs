﻿using System;
using System.Windows;
using TreeViewTest.View;
using TreeViewTest.ViewModel;

namespace TreeViewTest
{
    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Application launching handler.
        /// </summary>
        private void AppOnStartup(object sender, StartupEventArgs e)
        {
            try
            {
                var view = new MainWindow { DataContext = new MainWindowViewModel() };
                view.ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
                throw;
            }
        }
    }
}
