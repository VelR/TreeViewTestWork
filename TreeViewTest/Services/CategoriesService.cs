﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TreeViewTest.Interfaces;
using TreeViewTest.Model;

namespace TreeViewTest.Services
{
    /// <summary>
    /// Class to work with the categories.
    /// </summary>
    public class CategoriesService : ICategoriesService
    {
        #region Fields

        /// <summary>
        /// Instance of the class "Random".
        /// </summary>
        private readonly Random _random;

        #endregion

        #region Props

        /// <summary>
        /// The last category number.
        /// </summary>
        private int LastNumber => Categories.Any() ? Categories.Max(c => c.Id) : 0;

        /// <summary>
        /// Returns all added categories.
        /// </summary>
        public ObservableCollection<Category> Categories { get; }

        #endregion

        #region Constructor

        /// <summary>
        /// Class to work with the categories.
        /// </summary>
        public CategoriesService()
        {
            Categories = new ObservableCollection<Category>();
            _random = new Random();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add category.
        /// </summary>
        public void Add()
        {
            Categories.Add(CreateFilledCategory());
        }

        /// <summary>
        /// Adding more categories.
        /// </summary>
        /// <param name="number">Number of categories added.</param>
        public void Add(int number)
        {
            for (var i = 0; i < number; i++)
            {
                Add();
            }
        }

        /// <summary>
        /// Creates a filled category.
        /// </summary>
        private Category CreateFilledCategory()
        {
            return new Category(LastNumber + 1) { Items = CreateItems() };
        }

        /// <summary>
        /// Creates a random collection Items.
        /// </summary>
        private IEnumerable<Item> CreateItems()
        {
            var items = new List<Item>();
            var itemCount = _random.Next(1, 10);

            for (var i = 0; i < itemCount; i++)
            {
                items.Add(new Item(GetUniqueRandomItemId(items)));
            }

            return (items.OrderBy(item => item.Id).ToList());
        }

        /// <summary>
        /// Gets the unique item collection id.
        /// </summary>
        private int GetUniqueRandomItemId(IReadOnlyCollection<Item> items)
        {
            var getRandomItemId = new Func<int>(() => _random.Next(1, 30));
            var uniqueItemId = getRandomItemId();

            while (items.Any(x => x.Id == uniqueItemId))
            {
                uniqueItemId = getRandomItemId();
            }

            return uniqueItemId;
        }

        #endregion
    }
}
